const utils = require('./utils'),
    Web3 = require('web3'),
    config = require('../../config'),
    contract = require('truffle-contract'),
    TokenContract = require('../../build/contracts/AKJToken.json'),
    /////////////////////////////////
// Web3
    web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/v3/655c5c4cf6d8486cafbe6535ffddbda6")),

/////////////////////////////////
// Constants
    Token = contract(TokenContract),
    akj = new web3.eth.Contract(Token.abi, '0x5ab2d437Ec6D8E52b2191eFAFD985826A73D97dE');


/////////////////////////////////
// The program
try {
    init()
}
catch (error) {
    log.warn("Application errored: ", new Date().toJSON());
    throw(e)
}

async function init() {

    /* Start. Connect and get status from latest iteration */
    try {
        console.log('Application started on: ', new Date().toJSON());
        const today = new Date() //
        await utils.setProviders(akj, Token);
        await utils.connectMongoose();
        let latestStatus = await utils.getLatestStatus();
        console.log("latest Status", latestStatus);

        /* First run ever, or db has been cleared. */
        if (!latestStatus) {

            /* Here is the list of accounts that are NEGATED (excluded from dividends payouts) */
            let accountsToBeNegated = config.negatedAccounts;
            for (let address of accountsToBeNegated){
                await utils.addNegatedAccount(address);
            }
            latestStatus = utils.createCleanStatus();
            await utils.runIteration(latestStatus);
        }

        /* Case 2: We are trying to run an iteration on same day, and ensure that iteration is completed */
        else if (latestStatus.timeStart.getDate() === today.getDate()
            && latestStatus.timeStart.getMonth() === today.getMonth()
            && latestStatus.timeStart.getFullYear() === today.getFullYear()) {
            await utils.finishIteration(latestStatus);
        }

        /* Case 3: Else, it means we have a normal iteration. */
        else {
            await utils.runIteration(latestStatus);
        }

        /* Finish. Reset defaultBlock. */
        web3.eth.defaultBlock = "latest";
    }
    catch (e) {
        console.log("Application error!");
        await utils.disconnectMongoose().then(function(){
            console.log(e);
            throw(e);
        });
        throw(e);
    }
    finally {
        console.log('Application exited on: ', new Date().toJSON());
        await utils.disconnectMongoose().then(function(){
            console.log('exiting...');
            process.exit(0);
        });

    }
}



/*
// Main flow.
async function runIteration(latestStatus) {
    // Start normal iteration of cron job.

    let timeStart = new Date()
    let _fromBlock = latestStatus.toBlock
    let _toBlock = await web3.eth.getBlockNumber()
    web3.eth.defaultBlock = _toBlock

    console.log("From Block: ", _fromBlock)
    console.log("Current block: ", _toBlock)
    console.log("Default Block: ", web3.eth.defaultBlock)

    let statusToSave = {
        timeStart: timeStart,
        fromBlock: _fromBlock,
        toBlock: _toBlock,
        date: transformToDateString(new Date())
    }


    await utils.updateStatusState(statusToSave, 0)

    // Step 1. Get all the accounts and save them to database
    let accounts = await utils.getAccounts(akj, _fromBlock, _toBlock)
    await utils.saveNewAccountsToDb(accounts, _toBlock, timeStart)
    await utils.updateStatusState(statusToSave, 1)
    console.log("Step 1 finished.")
    log.trace("Completed step 1 of normal iteration: ", new Date().toJSON())

    // Step 2. Get all balances and save summary to database
    let accountsAndBalances = await utils.getBalances()
    await utils.saveDailyBalancesToDb(accountsAndBalances, _fromBlock, _toBlock)
    statusToSave.numberOfAccounts = Object.keys(accountsAndBalances).length
    await utils.updateStatusState(statusToSave, 2)
    console.log("Step 2 finished.")
    log.trace("Completed step 2 of normal iteration: ", new Date().toJSON())

    // Step 3. Create a csv and send over email.
    await utils.generateReportFromDailyBalance()
    await utils.updateStatusState(statusToSave, 3)
    console.log("Step 3 finished.")
    log.trace("Completed step 3 of normal iteration: ", new Date().toJSON())

    // Step 4. Create a status report and save to database for next run.
    await utils.sendMailDaily()
    statusToSave.timeEnd = new Date()
    await utils.updateStatusState(statusToSave, 4)
    console.log("Step 4 finished.")
    log.trace("Completed step 4 of normal iteration: ", new Date().toJSON())

    return statusToSave.state
}

// Finish iteration that has previously been started
async function finishIteration(latestStatus) {
    if (latestStatus.state !== 4) {
        log.trace("Continuing old iteration on: ", new Date().toJSON())
    }
    // Get same variables from previous iteration.
    let _fromBlock = latestStatus.fromBlock
    let _toBlock = latestStatus.toBlock
    web3.eth.defaultBlock = _toBlock
    let accountsAndBalances = {}

    console.log("From Block: ", _fromBlock)
    console.log("Current block: ", _toBlock)
    console.log("Default Block: ", web3.eth.defaultBlock)

    // Step 1. Get all the accounts and save them to database
    if (!latestStatus.state || latestStatus.state === 0) {
        let accounts = await getAccounts(akj, _fromBlock, _toBlock)
        await saveNewAccountsToDb(accounts, _toBlock, latestStatus.timeStart)
        await updateStatusState(latestStatus, 1)
        log.trace("Completed step 1 of old iteration: ", new Date().toJSON())
    }

    // Step 2. Get all balances and save summary to database
    if (latestStatus.state === 1) {
        accountsAndBalances = await getBalances()
        await saveDailyBalancesToDb(accountsAndBalances, _fromBlock, _toBlock)
        await updateStatusState(latestStatus, 2)
        log.trace("Completed step 2 of old iteration: ", new Date().toJSON())
    }

    // Step 3. Create a csv and send over email.
    if (latestStatus.state === 2) {
        await generateReportFromDailyBalance()
        await updateStatusState(latestStatus, 3)
        log.trace("Completed step 3 of old iteration: ", new Date().toJSON())
    }

    // Step 4. Create a status report and save to database for next run.
    if (latestStatus.state === 3) {
        await sendMailDaily()
        console.log("mail sent.")
        latestStatus.timeEnd = new Date()
        await updateStatusState(latestStatus, 4)
        log.trace("Completed step 4 of old iteration: ", new Date().toJSON())
    }

    return latestStatus.state
}

// Sets the providers for all input web3 contracts that are injected
async function setProviders(...args) {
    for (let i = 0; i < args.length; i++) {
        await setProvider(args[i])
    }

    async function setProvider(arg) {
        arg.setProvider(web3.currentProvider)
        if (typeof arg.currentProvider.sendAsync !== "function") {
            arg.currentProvider.sendAsync = function () {
                return arg.currentProvider.send.apply(
                    arg.currentProvider, arguments
                )
            }
        }
    }
}

async function getLatestStatus() {
    return await Status.findOne({}, {}, {sort: {timeEnd: -1}})
}

async function getNegatedAccounts() {
    return new Promise((resolve, reject) => {
        Account.find({negated: true}, {}, {}, (error, obj) => {
            resolve(obj)
        })
    })
}

/!* Scan ethereum blockchain for all accounts that have ever made AKJ Token transactions. *!/
async function getAccounts(contractInstance, fromBlock, toBlock) {
    let accounts = {}
    let events
    try {
        /!* Scan the AKJ Token smart contract for events emitted on Transfer *!/
        events = await contractInstance.getPastEvents('Transfer', {filter: {}, fromBlock, toBlock})
    }
    catch (e) {
        console.log(e)
        throw e
    }
    for (let i = 0; i < events.length; i++) {
        let eventObj = events[i]
        if (eventObj.returnValues.to === (emptyAddr)) {
            continue
        }
        if (!accounts[eventObj.returnValues.to]) {
            accounts[eventObj.returnValues.to] = true
        }
    }
    return Object.keys(accounts)
}

async function saveNewAccountsToDb(accounts, blockNumber, date) {
    let negatedAccounts = await getNegatedAccounts()
    for (let account of accounts) {
        let query = {address: account.toLowerCase()}
        let update = {
            address: account.toLowerCase(),
            block: blockNumber,
            date: date
        }
        if (negatedAccounts.includes(account)) {
            continue
        }
        let options = {upsert: true}
        await Account.findOneAndUpdate(query, update, options)
    }
}

/!* Check the current balance of all accounts found. *!/
async function getBalances() {
    let allAccounts = await Account.distinct("address")
    console.log("allAccounts")
    console.log(allAccounts)

    let accountsAndBalances = {}

    if (!akj) {
        return
    }
    for (let i = 0; i < allAccounts.length; i++) {
        let accountAddress = allAccounts[i]
        let latestBalance = await akj.methods.balanceOf(accountAddress).call()
        // let ethLatestBalance = web3.utils.fromWei(latestBalance,'ether')
        if (latestBalance === undefined || latestBalance === "0" || latestBalance === null || latestBalance === 0) continue
        accountsAndBalances[accountAddress] = latestBalance
    }
    return accountsAndBalances
}

/!* Save the current state of all balances and accounts to database *!/
async function saveDailyBalancesToDb(accountsAndBalances, fromBlock, toBlock) {
    console.log("dailyBalance")
    let totalSupply = await akj.methods.totalSupply().call()
    let totalSupplyBigNumber = new BigNumber(totalSupply)
    let dateString = transformToDateString(new Date())
    let accounts = Object.keys(accountsAndBalances)
    let dailyBalance = {
        date: dateString, // yyyy-mm-dd
        fullDate: new Date(),
        fromBlock: fromBlock,
        toBlock: toBlock,
        balances: {},
        totalSupply: totalSupply
    }

    let totalSupplyNegatedBigNumber = new BigNumber(0)
    for (let i = 0; i < accounts.length; i++) {
        let isNegated = (await Account.count({address: accounts[i], negated: true}) > 0)
        if (isNegated) {
            let logBalance = new BigNumber(accountsAndBalances[accounts[i]])
            console.log("Negated: ", logBalance.toString(10))
            totalSupplyNegatedBigNumber = totalSupplyNegatedBigNumber.plus(new BigNumber(accountsAndBalances[accounts[i]]))
        } else {
            dailyBalance.balances[accounts[i]] = accountsAndBalances[accounts[i]]
        }
    }
    dailyBalance['totalSupplyNegated'] = totalSupplyNegatedBigNumber.toString(10) // ERROR? New changes here
    let totalSupplyNotNegatedBigNumber = new BigNumber(totalSupplyBigNumber.minus(totalSupplyNegatedBigNumber))
    dailyBalance['totalSupplyNotNegated'] = totalSupplyNotNegatedBigNumber.toString(10)

    let options = {upsert: true}
    await DailyBalances.findOneAndUpdate({date: dateString}, dailyBalance, options)
}

async function sendMailDaily() {
    let file = base64_encode('./balances/' + getMostRecentFileName())
    const msg = {
        to: 'mb@akj.com',
        from: 'akjtoken@akj.com',
        subject: 'Latest AKJ Token transaction report: ' + transformToDateString(new Date()),
        text: 'AKJ Token Report\n\nFind the attached file on AKJ Token Transactions and Balances.\nThe report includes all balances of Ethereum Accounts that hold AKJ Tokens, excluding the accounts that are to be negated in the dividends payout.',
        attachments: [
            {
                filename: 'AKJreport_' + transformToDateString(new Date()) + '.csv',
                content: file
            }
        ]
    }
    console.log("MAIL API", process.env.SENDGRID_API_KEY)
    await sgMail.send(msg)
    console.log("Mail has been sent.")

    function base64_encode(file) {
        var bitmap = fs.readFileSync(file)
        return new Buffer(bitmap).toString('base64')
    }
}


/!* Create a CSV file with the accounts and balances from latest iteration *!/
async function generateReportFromDailyBalance() {
    let dailyBalanceObject = await DailyBalances.findOne({date: transformToDateString(new Date())}) // Retrieve all accounts from "Date"
    let currentDate = new Date()
    let finalPathFile = "./balances/balances" + currentDate.getFullYear() + "-" + (currentDate.getMonth() + 1) + "-" + currentDate.getUTCDate() + ".csv"
    let records = []
    const createCsvWriter = require('csv-writer').createObjectCsvWriter

    const newCsvWriter = createCsvWriter({
        path: finalPathFile,
        header: [
            {
                id: 'address', title: 'address'
            },
            {
                id: 'balance', title: 'balance'
            }
        ]
    })

    let dailyBalanceArray = Array.from(dailyBalanceObject.balances.keys())
    console.log("dailyBalanceArray", dailyBalanceArray)

    dailyBalanceArray.forEach(async (k) => {
        records.push({address: k, balance: dailyBalanceObject.balances.get(k)})
    })

    await newCsvWriter.writeRecords(records).then(function () {
        console.log("Done writing Daily CSV report with rows = ", records.length)
    })
}

/!* UTILITY FUNCTIONS *!/

function getMostRecentFileName() {
    let dir = './balances'
    let files = fs.readdirSync(dir)

    return _.max(files, function (f) {
        let fullpath = path.join(dir, f)
        return fs.statSync(fullpath).mtime
    })
}

function transformToDateString(date) {
    return ('0' + date.getDate()).slice(-2) + '-'
        + ('0' + (date.getMonth() + 1)).slice(-2) + '-'
        + date.getFullYear()
}

function createCleanStatus() {
    return {
        timeStart: new Date(),
        fromBlock: "0",
        toBlock: "0",
        firstBlock: "0",
        state: 0,
        date: new Date()
    }
}

async function updateStatusState(status, number) {
    status.state = number
    let query = {timeStart: status.timeStart}
    let options = {upsert: true}
    await Status.findOneAndUpdate(query, status, options)
}

async function addNegatedAccount(_address) {
    let toBeSaved = {
        address: _address.toLowerCase(),
        negated: true
    }
    let query = {address: _address}
    let options = {upsert: true}
    await Account.findOneAndUpdate(query, toBeSaved, options)
}
*/
