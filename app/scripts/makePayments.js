const Web3Import = require('web3')
let Web3 = new Web3Import(new Web3Import.providers.HttpProvider("http://localhost:8545"))
const contract = require('truffle-contract')
const TokenContract = require('../../build/contracts/AKJToken.json')
const BigNumber = require('bignumber.js')
const Token = contract(TokenContract)
const akj = new Web3.eth.Contract(Token.abi, '0xcb47279be3fdaf42aa7f2b3573b9852e3cf520ce') // AKJToken v2 Truffle Develop


run()

async function run() {
    await setProviders(akj, Token);

    let payouts = {
        "0xc08c763599513225f2c25405a5e1bd961aa3fc67": "1354234534",
        "0xda4333a62e31e8adf05001cf36a74efc94f16543": "212352123"
    }

    await makePayments(payouts)
}

// CALL FUNCTION WITH ADDRESS->WEI MAPPING, AND WILL PAY OUT TO ALL ADDRESSES
async function makePayments(addressToAmountMap) {
    let addresses = Object.keys(addressToAmountMap)
    for (let i=0; i<addresses.length; i++) {
        await transferWeiToAddress(addresses[i], addressToAmountMap[addresses[i]])
    }
}

//
async function transferWeiToAddress(address, amountInWei) {
    let tx = {
        from: process.env.ADDRESS,
        to: address,
        value: amountInWei
    }
    let amountInWeiBIG = new BigNumber(amountInWei)
    let estimatedGas = await Web3.eth.estimateGas(tx)
    let estimatedGasBIG = new BigNumber(estimatedGas)
    let newTxValue = amountInWeiBIG.minus(estimatedGasBIG)
    tx.value = newTxValue.toString(10)
    tx.gas = estimatedGas

    let signedTx = await Web3.eth.accounts.signTransaction(tx, process.env.PRIVATE_KEY)

    console.log(signedTx)

    Web3.eth.sendSignedTransaction(signedTx.rawTransaction)
        .on('receipt', function (receipt) {
            console.log("receipt", receipt)
        })
        .on('error', function (error) {
            console.log("error", error)
            // TODO: Handle some error -- what if transfer doesn't work on some % of addresses?
        })
}

async function setProviders(...args) {
    for (let i = 0; i < args.length; i++) {
        await setProvider(args[i])
    }

    async function setProvider(arg) {
        arg.setProvider(Web3.currentProvider)
        if (typeof arg.currentProvider.sendAsync !== "function") {
            arg.currentProvider.sendAsync = function () {
                return arg.currentProvider.send.apply(
                    arg.currentProvider, arguments
                )
            }
        }
    }
}