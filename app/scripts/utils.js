/////////////////////////////////
// IMPORTS
const config = require('../../config'),
    /*
    * MongoDb
    * */
    mongoose = require('mongoose'),
    mongo_db = config.dbName,
    mongo_user = config.dbUser,
    mongo_pass = config.dbPassword,
    mongo_port = config.dbPort,
    mongo_host = config.dbHost,
    /*
    * Models
    * */
    Account = require('../models/account'),
    DailyBalances = require('../models/dailyBalances'),
    QuarterlyDividends = require('../models/quarterDividends'),
    Payout = require('../models/payouts'),
    Status = require('../models/status'),
    /*
    *Web3 and contracts
    * */
    Web3 = require('web3'),
    contract = require('truffle-contract'),
    TokenContract = require('../../build/contracts/AKJToken.json'),
    Token = contract(TokenContract),
    web3 = new Web3(new Web3.providers.HttpProvider(config.ethProvider)),
    akj = new web3.eth.Contract(Token.abi, '0x5ab2d437Ec6D8E52b2191eFAFD985826A73D97dE'), // AKJToken v2 Main
    // akj = new web3.eth.Contract(Token.abi, '0xfe2de08f1c40761ff68d5b4dca78ef1cdc6cc2b9') // AKJToken v2 Truffle
    emptyAddr = '0x0000000000000000000000000000000000000000',
    /*
    * Utilities
    * */
    delay = require('delay'),
    BigNumber = require('bignumber.js'),
    Moment = require('moment'),
    fs = require('fs'),
    path = require('path'),
    _ = require('underscore'),
    csvToJson = require('csvtojson'),
    /*
    * Logger
    * */
    opts = {
        errorEventName: 'error',
        logDirectory: './logs',
        fileNamePattern: 'log-<DATE>.log',
        dateFormat: 'YYYY.MM.DD'
    },
    log = require('simple-node-logger').createRollingFileLogger(opts),

    // using SendGrid's v3 Node.js Library
    // https://github.com/sendgrid/sendgrid-nodejs
    // sgMail = require('@sendgrid/mail');
    // sgMail.setApiKey("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")

    //send mail
    nodeoutlook = require('nodejs-nodemailer-outlook'),
    nodemailer = require("nodemailer");


/////////////////////////////////

class utils {

    static async runIteration(latestStatus) {
        // Start normal iteration of cron job.

        let me = this,
            timeStart = new Date(),
            _fromBlock = latestStatus.toBlock,
            _toBlock = await web3.eth.getBlockNumber();

        web3.eth.defaultBlock = _toBlock;

        console.log("From Block: ", _fromBlock);
        console.log("Current block: ", _toBlock);
        console.log("Default Block: ", web3.eth.defaultBlock);

        let statusToSave = {
            timeStart: timeStart,
            fromBlock: _fromBlock,
            toBlock: _toBlock,
            date: me.transformToDateString(new Date())
        };


        await me.updateStatusState(statusToSave, 0);

        // Step 1. Get all the accounts and save them to database
        let accounts = await me.getAccounts(akj, _fromBlock, _toBlock);
        await me.saveNewAccountsToDb(accounts, _toBlock, timeStart);
        await me.updateStatusState(statusToSave, 1);
        console.log("Step 1 finished.");
        log.trace("Completed step 1 of normal iteration: ", new Date().toJSON());

        // Step 2. Get all balances and save summary to database
        let accountsAndBalances = await me.getBalances();
        await me.saveDailyBalancesToDb(accountsAndBalances, _fromBlock, _toBlock);
        statusToSave.numberOfAccounts = Object.keys(accountsAndBalances).length;
        await me.updateStatusState(statusToSave, 2);
        console.log("Step 2 finished.");
        log.trace("Completed step 2 of normal iteration: ", new Date().toJSON());

        // Step 3. Create a csv and send over email.
        await me.generateReportFromDailyBalance();
        await me.updateStatusState(statusToSave, 3);
        console.log("Step 3 finished.");
        log.trace("Completed step 3 of normal iteration: ", new Date().toJSON());

        // Step 4. Create a status report and save to database for next run.
        // await me.sendMailDaily();
        statusToSave.timeEnd = new Date();
        await me.updateStatusState(statusToSave, 4);
        console.log("Step 4 finished.");
        log.trace("Completed step 4 of normal iteration: ", new Date().toJSON());

        return statusToSave.state;
    }

// Finish iteration that has previously been started
    static async finishIteration(latestStatus) {
        let me = this;

        if (latestStatus.state !== 4) {
            log.trace("Continuing old iteration on: ", new Date().toJSON());
        }
        // Get same variables from previous iteration.
        let _fromBlock = latestStatus.fromBlock,
            _toBlock = latestStatus.toBlock;

        web3.eth.defaultBlock = _toBlock;
        let accountsAndBalances = {};

        console.log("From Block: ", _fromBlock);
        console.log("Current block: ", _toBlock);
        console.log("Default Block: ", web3.eth.defaultBlock);

        // Step 1. Get all the accounts and save them to database
        if (!latestStatus.state || latestStatus.state === 0) {
            let accounts = await me.getAccounts(akj, _fromBlock, _toBlock);
            await me.saveNewAccountsToDb(accounts, _toBlock, latestStatus.timeStart);
            await me.updateStatusState(latestStatus, 1);
            log.trace("Completed step 1 of old iteration: ", new Date().toJSON());
        }

        // Step 2. Get all balances and save summary to database
        if (latestStatus.state === 1) {
            accountsAndBalances = await getBalances();
            await me.saveDailyBalancesToDb(accountsAndBalances, _fromBlock, _toBlock);
            await me.updateStatusState(latestStatus, 2);
            log.trace("Completed step 2 of old iteration: ", new Date().toJSON());
        }

        // Step 3. Create a csv and send over email.
        if (latestStatus.state === 2) {
            await me.generateReportFromDailyBalance();
            await me.updateStatusState(latestStatus, 3);
            log.trace("Completed step 3 of old iteration: ", new Date().toJSON());
        }

        // Step 4. Create a status report and save to database for next run.
        if (latestStatus.state === 3) {
            /*await me.sendMailDaily();
            console.log("mail sent.");*/
            latestStatus.timeEnd = new Date();
            await me.updateStatusState(latestStatus, 4);
            log.trace("Completed step 4 of old iteration: ", new Date().toJSON());
        }

        return latestStatus.state
    }


    static async setProviders(...args) {
        for (let i = 0; i < args.length; i++) {
            await setProvider(args[i]);
        }

        async function setProvider(arg) {
            arg.setProvider(web3.currentProvider);
            if (typeof arg.currentProvider.sendAsync !== "function") {
                arg.currentProvider.sendAsync = function () {
                    return arg.currentProvider.send.apply(
                        arg.currentProvider, arguments
                    )
                }
            }
        }
    }

    static async getLatestStatus() {
        return await Status.findOne({}, {}, {sort: {timeEnd: -1}})
    }

    static async getNegatedAccounts() {
        return new Promise((resolve, reject) => {
            Account.find({negated: true}, {}, {}, (error, obj) => {
                resolve(obj)
            })
        })
    }

    /* Scan ethereum blockchain for all accounts that have ever made AKJ Token transactions. */
    static async getAccounts(contractInstance, fromBlock, toBlock) {
        let accounts = {},
            events;

        try {
            /* Scan the AKJ Token smart contract for events emitted on Transfer */
            events = await contractInstance.getPastEvents('Transfer', {filter: {}, fromBlock, toBlock});
        }
        catch (e) {
            console.log(e);
            throw e;
        }
        for (let i = 0; i < events.length; i++) {
            let eventObj = events[i];
            if (eventObj.returnValues.to === (emptyAddr)) {
                continue
            }
            if (!accounts[eventObj.returnValues.to]) {
                accounts[eventObj.returnValues.to] = true;
            }
        }
        return Object.keys(accounts)
    }

    static async saveNewAccountsToDb(accounts, blockNumber, date) {
        let negatedAccounts = await this.getNegatedAccounts();

        for (let account of accounts) {
            let query = {address: account.toLowerCase()};
            let update = {
                address: account.toLowerCase(),
                block: blockNumber,
                date: date
            };
            if (negatedAccounts.includes(account)) {
                continue
            }
            let options = {upsert: true};
            await Account.findOneAndUpdate(query, update, options);
        }
    }

    /* Check the current balance of all accounts found. */
    static async getBalances() {
        let allAccounts = await Account.distinct("address");

        console.log("allAccounts");
        console.log(allAccounts);

        let accountsAndBalances = {};

        if (!akj) {
            return
        }

        for (let i = 0; i < allAccounts.length; i++) {
            let accountAddress = allAccounts[i];
            let latestBalance = await akj.methods.balanceOf(accountAddress).call();
            // let ethLatestBalance = web3.utils.fromWei(latestBalance,'ether')
            if (latestBalance === undefined || latestBalance === "0" || latestBalance === null || latestBalance === 0) continue;

            accountsAndBalances[accountAddress] = latestBalance;
        }
        return accountsAndBalances
    }

    /* Save the current state of all balances and accounts to database */
    static async saveDailyBalancesToDb(accountsAndBalances, fromBlock, toBlock) {
        console.log("dailyBalance");
        let totalSupply = await akj.methods.totalSupply().call();
        let totalSupplyBigNumber = new BigNumber(totalSupply);
        let dateString = this.transformToDateString(new Date());
        let accounts = Object.keys(accountsAndBalances);
        let dailyBalance = {
            date: dateString, // yyyy-mm-dd
            fullDate: new Date(),
            fromBlock: fromBlock,
            toBlock: toBlock,
            balances: {},
            totalSupply: totalSupply
        }

        let totalSupplyNegatedBigNumber = new BigNumber(0);
        for (let i = 0; i < accounts.length; i++) {
            let isNegated = (await Account.count({address: accounts[i], negated: true}) > 0);
            if (isNegated) {
                let logBalance = new BigNumber(accountsAndBalances[accounts[i]]);
                console.log("Negated: ", logBalance.toString(10));
                totalSupplyNegatedBigNumber = totalSupplyNegatedBigNumber.plus(new BigNumber(accountsAndBalances[accounts[i]]));
            } else {
                dailyBalance.balances[accounts[i]] = accountsAndBalances[accounts[i]];
            }
        }
        dailyBalance['totalSupplyNegated'] = totalSupplyNegatedBigNumber.toString(10); // ERROR? New changes here
        let totalSupplyNotNegatedBigNumber = new BigNumber(totalSupplyBigNumber.minus(totalSupplyNegatedBigNumber));
        dailyBalance['totalSupplyNotNegated'] = totalSupplyNotNegatedBigNumber.toString(10);

        let options = {upsert: true};
        await DailyBalances.findOneAndUpdate({date: dateString}, dailyBalance, options);
    }

    static async sendMailDaily() {
        let me = this,
            path = './balances/' + me.getMostRecentFileName('./balances');
        let msg = {
            auth: {
                user: "optracker@akj.com",
                pass: "W*0!Ha:8rJn"
            },
            to: 'token-distribution-reports@akj.com',
            from: 'optracker@akj.com',
            subject: 'Latest AKJ Token transaction report: ' + me.transformToDateString(new Date()),
            text: 'AKJ Token Report\n\nFind the attached file on AKJ Token Transactions and Balances.\nThe report includes all balances of Ethereum Accounts that hold AKJ Tokens, excluding the accounts that are to be negated in the dividends payout.',
            attachments: [
                {
                    filename: 'AKJreport_' + me.transformToDateString(new Date()) + '.csv',
                    path: path

                }
            ],
            onError: (e) => console.log(e),
            onSuccess: console.log("Mail has been sent.")
        };
        nodeoutlook.sendEmail(msg);


        function base64_encode(file) {
            var bitmap = fs.readFileSync(file);
            return new Buffer(bitmap).toString('base64')
        }
    }


    /* Create a CSV file with the accounts and balances from latest iteration */
    static async generateReportFromDailyBalance() {
        let me = this,
            dailyBalanceObject = await DailyBalances.findOne({date: me.transformToDateString(new Date())}), // Retrieve all accounts from "Date"
            currentDate = new Date(),
            finalPathFile = "./balances/balances" + currentDate.getFullYear() + "-" + (currentDate.getMonth() + 1) + "-" + currentDate.getUTCDate() + ".csv",
            records = [];
        const createCsvWriter = require('csv-writer').createObjectCsvWriter;

        const newCsvWriter = createCsvWriter({
            path: finalPathFile,
            header: [
                {
                    id: 'address', title: 'address'
                },
                {
                    id: 'balance', title: 'balance'
                }
            ]
        })

        let dailyBalanceArray = Array.from(dailyBalanceObject.balances.keys());
        console.log("dailyBalanceArray", dailyBalanceArray);

        dailyBalanceArray.forEach(async (k) => {
            records.push({address: k, balance: Web3.utils.fromWei(dailyBalanceObject.balances.get(k), 'ether')});
        });

        await newCsvWriter.writeRecords(records).then(function () {
            console.log("Done writing Daily CSV report with rows = ", records.length);
        })
    }

    /* UTILITY FUNCTIONS */

    static getMostRecentFileName(dir) {
        let files = fs.readdirSync(dir);

        return _.max(files, function (f) {
            let fullpath = path.join(dir, f);
            return fs.statSync(fullpath).mtime
        })
    }

    static transformToDateString(date) {
        return ('0' + date.getDate()).slice(-2) + '-'
            + ('0' + (date.getMonth() + 1)).slice(-2) + '-'
            + date.getFullYear()
    }

    static createCleanStatus() {
        return {
            timeStart: new Date(),
            fromBlock: "0",
            toBlock: "0",
            firstBlock: "0",
            state: 0,
            date: new Date()
        }
    }

    static async updateStatusState(status, number) {
        status.state = number;
        let query = {timeStart: status.timeStart},
            options = {upsert: true};

        await Status.findOneAndUpdate(query, status, options)
    }

    static async addNegatedAccount(_address) {
        let toBeSaved = {
            address: _address.toLowerCase(),
            negated: true
        }
        let query = {address: _address},
            options = {upsert: true};

        await Account.findOneAndUpdate(query, toBeSaved, options)
    }
/*
=================================================================================================================================================================
quarterly dividents start
=================================================================================================================================================================
*/
    static async buildQuarterlyDividendsReport() {
        let me = this,
            // date = new Date("06-06-2020"), // TODO: Should be run "Today"
            date = new Date(), // TODO: Should be run "Today"
            moment = new Moment(date),
            previousQuarter = me.getPreviousQuarterFromInputMoment(moment),

            dbQuarterlyDividends = {
            year: previousQuarter.year,
            quarter: previousQuarter.quarter,
            balances: {},
            percentages: {},
            amounts: {}
            },
            quarterlyAccountsAndBalancesDto = {
                quarterlyTotalSupply: new BigNumber(0),
                quarterlyTotalSupplyNegated: new BigNumber(0),
                quarterlyTotalSupplyNotNegated: new BigNumber(0)
            };

        await me.upsertQuarterlyDividendsToDb(dbQuarterlyDividends);

        while (previousQuarter.startDate.isBefore(previousQuarter.endDate)) {
            await me.aggregateDailyBalancesToQuarterlyBalance(previousQuarter.startDate, quarterlyAccountsAndBalancesDto, dbQuarterlyDividends)
            previousQuarter.startDate.add(1, 'days')
        }

        dbQuarterlyDividends.totalSupply = quarterlyAccountsAndBalancesDto.quarterlyTotalSupply.toString(10)
        dbQuarterlyDividends.totalSupplyNegated = quarterlyAccountsAndBalancesDto.quarterlyTotalSupplyNegated.toString(10)
        dbQuarterlyDividends.totalSupplyNotNegated = quarterlyAccountsAndBalancesDto.quarterlyTotalSupplyNotNegated.toString(10)

        await me.upsertQuarterlyDividendsToDb(dbQuarterlyDividends);
        console.log("Quarterly dividends object saved and ready to continue ...");

        console.log("Calculating dividends for the quarter ...");
        await me.calculatePercentagesAndAmountsFromBalances(previousQuarter.year, previousQuarter.quarter);
        console.log("Complete: Calculating dividends for the quarter ...");
    }

    static async aggregateDailyBalancesToQuarterlyBalance(targetDateAsMoment, quarterlyAccountsAndBalancesDto, dbQuarterlyDividends) {
        let dailyBalance = await DailyBalances.findOne({date: targetDateAsMoment.format("DD-MM-YYYY")})

        if (dailyBalance !== null) {
            console.log("Date being processed: ", targetDateAsMoment.format("DD-MM-YYYY"))

            let totalSupplyBig = new BigNumber(dailyBalance.get("totalSupply"))
            let totalSupplyNegatedBig = new BigNumber(dailyBalance.get("totalSupplyNegated"))
            let totalSupplyNotNegatedBig = new BigNumber(dailyBalance.get("totalSupplyNotNegated"))
            quarterlyAccountsAndBalancesDto.quarterlyTotalSupply = quarterlyAccountsAndBalancesDto.quarterlyTotalSupply.plus(totalSupplyBig)
            quarterlyAccountsAndBalancesDto.quarterlyTotalSupplyNegated = quarterlyAccountsAndBalancesDto.quarterlyTotalSupplyNegated.plus(totalSupplyNegatedBig)
            quarterlyAccountsAndBalancesDto.quarterlyTotalSupplyNotNegated = quarterlyAccountsAndBalancesDto.quarterlyTotalSupplyNotNegated.plus(totalSupplyNotNegatedBig)


            let accounts = dailyBalance.balances.keys();

            for (let account of accounts) {
                if (dbQuarterlyDividends.balances[account]) {
                    let oldAccumulatedValue = new BigNumber(dbQuarterlyDividends.balances[account])
                    let dailyValue = new BigNumber(dailyBalance.balances.get(account))
                    let newAccumulatedValue = new BigNumber(oldAccumulatedValue.plus(dailyValue))
                    dbQuarterlyDividends.balances[account] = newAccumulatedValue.toString(10)
                }
                else {
                    let dailyValue = new BigNumber(dailyBalance.balances.get(account))
                    dbQuarterlyDividends.balances[account] = dailyValue.toString(10)
                }
            }
        }
    }

    static async calculatePercentagesAndAmountsFromBalances(year, quarter) {
        let dbQuarterlyDividends = await QuarterlyDividends.findOne({ // TODO: This is unecessary, we can re-use from (buildQuarterlyDividendsObject)
                year: year,
                quarter: quarter
            }),
            me = this;


        if (dbQuarterlyDividends !== null) {

            console.log("Building address to percentage mapping ...");
            let addressesToPercentagesMap = await me.buildAddressToPercentageMap(dbQuarterlyDividends);
            dbQuarterlyDividends.percentages = addressesToPercentagesMap;

            await QuarterlyDividends.update({
                year: dbQuarterlyDividends.year,
                quarter: dbQuarterlyDividends.quarter
            }, dbQuarterlyDividends);
            console.log("Complete: Building address to percentage mapping ...");

            console.log("Building address to payout-amount mapping ...");
            let totalPayoutAmount = await me.getTotalAmountToPayOut(addressesToPercentagesMap);
            console.log("Total amount to pay out: ", totalPayoutAmount);
            let addressToAmountMap = await me.buildAddressToAmountMap(addressesToPercentagesMap, totalPayoutAmount);
            dbQuarterlyDividends.amounts = addressToAmountMap;
            console.log("Payout datetime: ", Moment().add(1, 'days').format('DD-MM-YYYY'));
            dbQuarterlyDividends.payoutTime = Moment(new Date()).add(1, 'days').format('DD-MM-YYYY');
            console.log("Updating quarterly dividends: ", dbQuarterlyDividends);
            console.log("Complete: Building address to payout-amount mapping ...");
            await QuarterlyDividends.update({
                year: dbQuarterlyDividends.year,
                quarter: dbQuarterlyDividends.quarter
            }, dbQuarterlyDividends);

        } else {
            throw new Error("Couldn't retrieve quarterly dividends.")
        }
    }

    static async buildAddressToPercentageMap(quarterlyDividends) {

        let addressesToPercentages = {};

        for (let addressToBalance of quarterlyDividends.balances) {
            let address = addressToBalance[0],
                balance = addressToBalance[1],

                // Check if negated, no negated account should be here.
                isNegated = await Account.count({address: address, negated: true});

            if (isNegated) {
                addressesToPercentages[address] = 0;
            } else {
                let totalSupplyNotNegatedBIG = new BigNumber(quarterlyDividends.totalSupplyNotNegated),
                    balanceBIG = new BigNumber(balance),
                    percentageBIG = balanceBIG.div(totalSupplyNotNegatedBIG);

                addressesToPercentages[address] = percentageBIG.toString(10);
                // addressesToPercentages[address] = percentageBIG.toString(10);

            }
        }
        let controlNumberForPercentage = 0;
        for (var  i in addressesToPercentages) {
            console.log(parseFloat(addressesToPercentages[i]));
            controlNumberForPercentage += parseFloat(addressesToPercentages[i]);
        }
        controlNumberForPercentage = this.round(controlNumberForPercentage, 12);

        if (controlNumberForPercentage !== 1) {
            console.log('percentage error. should be 1. now is: '+controlNumberForPercentage);
            process.exit()
        }


        console.log('addressesToPercentages: ', addressesToPercentages);
        return addressesToPercentages
    }

    static  round(value, precision) {
        var multiplier = Math.pow(10, precision || 0);
        return Math.round(value * multiplier) / multiplier;
    }

    static async buildAddressToAmountMap(addressToPercentageMap, totalAmount) {
        for (let address in addressToPercentageMap) {
            console.log("Total is", totalAmount, " Amount is", addressToPercentageMap[address]);
            let totalAmountBig = new BigNumber(totalAmount),
                percentageBig = new BigNumber(addressToPercentageMap[address]);

            addressToPercentageMap[address] = Math.floor(totalAmountBig.times(percentageBig)) //TODO: Math.floor takes Number as argument. We're sending a BigNumber - may cause problems in the future?
        }
        return addressToPercentageMap
    }

    static async upsertQuarterlyDividendsToDb(quarterlyDividends) {
        let query = {year: quarterlyDividends.year, quarter: quarterlyDividends.quarter},
            options = {upsert: true};

        await QuarterlyDividends.findOneAndUpdate(query, quarterlyDividends, options)
    }

    static async getQuarterlyDividents(year, quarter){
        return await QuarterlyDividends.findOne({
            year: year,
            quarter: quarter
        });
    }

    static async updateQuarterlyDividents(quarterlyDividends){
        return await QuarterlyDividends.update({
            year: quarterlyDividends.year,
            quarter: quarterlyDividends.quarter
        }, quarterlyDividends)
    }

    static getPreviousQuarterFromInputMoment(moment) {
        if (moment.quarter() === 1) {
            moment.year(moment.year() - 1);
            moment.quarter(4);
        } else {
            moment.quarter(moment.quarter() - 1);
        }

        return ({
            startDate: Moment(moment.startOf('quarter')),
            endDate: Moment(moment.endOf('quarter')),
            quarter: moment.quarter(),
            year: moment.year()
        })
    }

    static async getTotalAmountToPayOut() {
        return await web3.eth.getBalance(process.env.ADDRESS)
    }

// CALL FUNCTION WITH ADDRESS->WEI MAPPING, AND WILL PAY OUT TO ALL ADDRESSES
    static async makePayments(quarterlyDividends) {
        let addressToAmountMap = quarterlyDividends.amounts,
            addresses = addressToAmountMap.keys();

        for (let address of addresses) {
            console.log("Payout to address: " + address + " with amount: " + addressToAmountMap.get(address));
            // await this.transferWeiToAddress(address, addressToAmountMap.get(address), quarterlyDividends);
        }
    }

//
    static async transferWeiToAddress(address, amountInWei, quarterlyDividends, nonce) {
        if (0 === parseInt(amountInWei)) { // TODO: May error? amountInWei could be bigger than Integer can hold.
            return
        }
        let tc = await web3.eth.getTransactionCount(process.env.ADDRESS,"pending");
        console.log(tc);

        let tx = {
            nonce: tc,
            from: process.env.ADDRESS,
            to: Web3.utils.toChecksumAddress(address),
            value: amountInWei
        },
            amountInWeiBIG = new BigNumber(amountInWei),
            estimatedGas = await web3.eth.estimateGas(tx),
            estimatedGasBIG = new BigNumber(estimatedGas),
            newTxValue = amountInWeiBIG.minus(estimatedGasBIG);

        tx.value = newTxValue.toString(10);
        tx.gas = estimatedGas + ((estimatedGas*10)/100);

        console.log("Send total: ", Web3.utils.fromWei(String(amountInWei), 'ether'));
        console.log("Minus gas: ", Web3.utils.fromWei(String(tx.value), 'ether'));

        let signedTx = await web3.eth.accounts.signTransaction(tx, process.env.PRIVATE_KEY);


        await web3.eth.sendSignedTransaction(signedTx.rawTransaction)
            .on('receipt', async function (receipt) { // TODO: Should function be async? Will it take longer executing all txs? Handle receipt in any other way?
                let payout = new Payout({
                    address: tx.to,
                    amount: amountInWei,
                    year: quarterlyDividends.year,
                    quarter: quarterlyDividends.quarter,
                    date: new Date(),
                    successful: true,
                    message: ""
                });
                await payout.save();

            })
            .on('error', async function (error) { // TODO: Should function be async?
                console.log("error", error);
                let payout = new Payout({
                    address: tx.to,
                    amount: amountInWei,
                    year: quarterlyDividends.year,
                    quarter: quarterlyDividends.quarter,
                    date: new Date(),
                    successful: false,
                    message: "" + error
                });
                await payout.save();
            })
    }

    static async sendMailQuarterly(previousQuarter) {
        await this.generateReportFromQuarterlyDividends(previousQuarter);
        var me = this,
            file = './quarterlyreports/' + me.getMostRecentFileName('./quarterlyreports');

        console.log("sending mail");
        return new Promise((resolve,reject)=>{
            let transporter = nodemailer.createTransport({
                service: "Outlook365",
                auth: {
                    user: config.outlook_user,
                    pass: config.outlook_pass
                }
            });
            var mailOptions = {
                from: config.outlook_user,
                to: config.outlook_recipient, // list of receivers
                subject: "AKJ Token Quarterly Dividends Report and Payout Warning", // Subject line
                text: 'AKJ Token Quarterly Dividends Report',
                html: "<h1>AKJ Token Quarterly Dividends Report</h1><p>The quarterly report on AKJ Token holders, their current balances, and their expected percentage and amounts to be paid out in dividends.</p><p>Please note that the payout will be executed in approximately 24 hours.</p>", // html body
                attachments: [
                    {
                        filename: 'AKJToken_QuarterReport_' + me.transformToDateString(new Date()) + '.csv',
                        path: file
                    }
                ]
            };

            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                    console.log("error is "+error);
                    resolve(false); // or use rejcet(false) but then you will have to handle errors
                }
                else {
                    console.log('Email sent: ' + info.response);
                    resolve(true);
                }
            });
        });

        // function to encode file data to base64 encoded string
        function base64_encode(file) {
            var bitmap = fs.readFileSync(file);
            return new Buffer(bitmap).toString('base64')
        }

    }

    static async generateReportFromQuarterlyDividends(previousQuarter) {
        let quarterlyDividends = await QuarterlyDividends.findOne({
            year: previousQuarter.year,
            quarter: previousQuarter.quarter
            // quarter: 4
        }); // Retrieve all accounts from "Date"

        let currentDate = new Date(),
            finalPathFile = './quarterlyreports/quarterlyReport_' + previousQuarter.year + "-Q" + previousQuarter.quarter + "_" + currentDate.getTime() + ".csv",
            records = [];

        const createCsvWriter = require('csv-writer').createObjectCsvWriter;

        const newCsvWriter = createCsvWriter({
            path: finalPathFile,
            header: [
                {
                    id: 'address', title: 'address'
                },
                {
                    id: 'balance', title: 'balance'
                },
                {
                    id: 'percentage', title: 'percentage'
                },
                {
                    id: 'amount', title: 'amount in ether'
                }
            ]
        });


        for (const address of quarterlyDividends.balances.keys()) {

            let balance_in_wei_big = new BigNumber(quarterlyDividends.balances.get(address)),
                balance_in_wei = balance_in_wei_big.toFixed(),
                balance_in_ether = Web3.utils.fromWei(balance_in_wei,'ether'),
                amount_in_wei_big = new BigNumber(quarterlyDividends.amounts.get(address)),
                amount_in_wei = amount_in_wei_big.toFixed(),
                amount_in_ether = Web3.utils.fromWei(amount_in_wei,'ether');

            // console.log(balance_in_ether, amount_in_ether);

            records.push({
                address: address,
                balance: balance_in_ether,
                percentage: quarterlyDividends.percentages.get(address),
                amount: amount_in_ether
            })
        }

        await newCsvWriter.writeRecords(records).then(function () {
            console.log("Done writing quarterly CSV report with rows = ", records.length)
        })
    }

    static async disconnectMongoose(){
        try{
            mongoose.disconnect();
        }catch (e) {
            log.trace('Error on disconnecting mongo: '+e);
        }
    }
    static async connectMongoose(){
        await mongoose.connect(`mongodb://${mongo_user}:${mongo_pass}@${mongo_host}:${mongo_port}/${mongo_db}`).then(() => console.log('MongoDB Connected..')).catch(err => console.log(err));
    }

}
module.exports = utils;
