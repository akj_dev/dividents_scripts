const mongoose = require('mongoose')

let Account = require('../models/account')
let Status = require('../models/status')
let DailyBalances = require('../models/dailyBalances')
let QuarterDividends = require('../models/quarterDividends')
let Payout = require('../models/payouts')


(async function() {
    await mongoose.connect('mongodb://localhost/akjtoken')
    Account.remove({}, function(error){
        console.log("account removed")
    })
    DailyBalances.remove({}, function(error){
            console.log("daily removed")
        })
    Status.remove({}, function(error){
        console.log("status removed")
    })
    QuarterDividends.remove({}, function(error){
        console.log("QuarterDividends removed")
    })
    Payout.remove({}, function(error){
        console.log("Payout removed")
    })
    await mongoose.disconnect()
}())
