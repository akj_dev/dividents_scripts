require('dotenv').config();
const utils = require('./utils'),
    Web3 = require('web3'),
    TokenContract = require('../../build/contracts/AKJToken.json'),
    /////////////////////////////////
// Web3
//     web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/v3/655c5c4cf6d8486cafbe6535ffddbda6")),
    web3 = new Web3(new Web3.providers.HttpProvider("https://rinkeby.etherscan.io/")), //testnet rinkeby

    contract = require('truffle-contract'),
    Token = contract(TokenContract),
    akj = new web3.eth.Contract(Token.abi, '0x5ab2d437Ec6D8E52b2191eFAFD985826A73D97dE'),
    Moment = require('moment');
/*const mongoose = require('mongoose')
const Web3Import = require('web3')
const contract = require('truffle-contract')
const TokenContract = require('../../build/contracts/AKJToken.json')
const BigNumber = require('bignumber.js')
const Moment = require('moment')
const fs = require('fs')
const path = require('path')
const csvToJson = require('csvtojson')
const _ = require('underscore')

// using SendGrid's v3 Node.js Library
// https://github.com/sendgrid/sendgrid-nodejs
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey("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")

let Account = require('../models/account')
let DailyBalances = require('../models/dailyBalances')
let QuarterlyDividends = require('../models/quarterDividends')
let Payout = require('../models/payouts')

let Web3 = new Web3Import(new Web3Import.providers.HttpProvider("https://mainnet.infura.io/v3/655c5c4cf6d8486cafbe6535ffddbda6"))
const Token = contract(TokenContract)
// const akj = new Web3.eth.Contract(Token.abi, '0xfe2de08f1c40761ff68d5b4dca78ef1cdc6cc2b9') // AKJToken v2 Truffle Develop
const akj = new Web3.eth.Contract(Token.abi, '0x5ab2d437Ec6D8E52b2191eFAFD985826A73D97dE') // AKJToken v2 Main*/

run()

async function run() {
    try {
        await utils.setProviders(akj, Token);
        await utils.connectMongoose();

        if (typeof process.env.ADDRESS !== "string" || typeof process.env.PRIVATE_KEY !== "string") {
            console.log("process.env.ADDRESS", process.env.ADDRESS);
            console.log("process.env.PRIVATE_KEY", process.env.PRIVATE_KEY);
            console.log("Set ADDRESS and PRIVATE_KEY ");
            process.exit();
        }

        // Rule 1 - Ethereum Account must have more than 1 ether to make payouts
        if (await utils.getTotalAmountToPayOut() < Web3.utils.toWei("1", 'ether')) {
            console.log("Will only run if address has more then 1 ether");
            process.exit();
        }

        // Rule 2 - Only pay out if time window has passed
        // let now = Moment().endOf('quarter').add(1, 'days') // TODO: This is for Development/debugging
        let now = Moment(); // TODO: This is for Production

        let previousQuarter = utils.getPreviousQuarterFromInputMoment(now);

        let quarterlyDividends = await utils.getQuarterlyDividents(previousQuarter.year, previousQuarter.quarter);
        //previousQuarter.quarter

        // If Quarterly Report doesn't exist --> Build Report
        if (quarterlyDividends === null || quarterlyDividends.payoutTime === null || quarterlyDividends.payoutTime === "") {
            console.log("Building quarterly dividends object from daily balances ...");
            await utils.buildQuarterlyDividendsReport();
            console.log("Complete: Building quarterly dividends object from daily balances ...");
            console.log("Starting to send report as email");
            await utils.sendMailQuarterly(previousQuarter);
            process.exit();
        }
        // Make the payments
        if (quarterlyDividends.payoutTime) {
            // let now = Moment()
            let payoutTime = Moment(quarterlyDividends.payoutTime, "DD-MM-YYYY");
            let now = Moment().add(2, 'd') // TODO: This is used for development / testing
            // let now = Moment();

            if (now.isAfter(payoutTime) && quarterlyDividends.hasBeenPaid !== true) {
                console.log("Starting payout");
                await utils.makePayments(quarterlyDividends);
                quarterlyDividends.hasBeenPaid = true;
                await utils.updateQuarterlyDividents(quarterlyDividends);
            }
            else {
                // Already paid out, nothing to do?
                console.log("Already been paid out")
            }
        }
    } catch (e) {
        console.log(e)
    } finally {
        await utils.disconnectMongoose();
        process.exit()
    }
}
/*

async function buildQuarterlyDividendsReport() {
    let date = new Date("02-02-2020") // TODO: Should be run "Today"
    // let date = new Date() // TODO: Should be run "Today"
    let moment = new Moment(date)
    let previousQuarter = getPreviousQuarterFromInputMoment(moment)

    let dbQuarterlyDividends = {
        year: previousQuarter.year,
        quarter: previousQuarter.quarter,
        balances: {},
        percentages: {},
        amounts: {}
    }
    await upsertQuarterlyDividendsToDb(dbQuarterlyDividends)

    var quarterlyAccountsAndBalancesDto = {
        quarterlyTotalSupply: new BigNumber(0),
        quarterlyTotalSupplyNegated: new BigNumber(0),
        quarterlyTotalSupplyNotNegated: new BigNumber(0)
    }
    while (previousQuarter.startDate.isBefore(previousQuarter.endDate)) {
        await aggregateDailyBalancesToQuarterlyBalance(previousQuarter.startDate, quarterlyAccountsAndBalancesDto, dbQuarterlyDividends)
        previousQuarter.startDate.add(1, 'days')
    }

    dbQuarterlyDividends.totalSupply = quarterlyAccountsAndBalancesDto.quarterlyTotalSupply.toString(10)
    dbQuarterlyDividends.totalSupplyNegated = quarterlyAccountsAndBalancesDto.quarterlyTotalSupplyNegated.toString(10)
    dbQuarterlyDividends.totalSupplyNotNegated = quarterlyAccountsAndBalancesDto.quarterlyTotalSupplyNotNegated.toString(10)

    await upsertQuarterlyDividendsToDb(dbQuarterlyDividends)
    console.log("Quarterly dividends object saved and ready to continue ...")

    console.log("Calculating dividends for the quarter ...")
    await calculatePercentagesAndAmountsFromBalances(previousQuarter.year, previousQuarter.quarter)
    console.log("Complete: Calculating dividends for the quarter ...")
}

async function aggregateDailyBalancesToQuarterlyBalance(targetDateAsMoment, quarterlyAccountsAndBalancesDto, dbQuarterlyDividends) {
    let dailyBalance = await DailyBalances.findOne({date: targetDateAsMoment.format("DD-MM-YYYY")})

    if (dailyBalance !== null) {
        console.log("Date being processed: ", targetDateAsMoment.format("DD-MM-YYYY"))

        let totalSupplyBig = new BigNumber(dailyBalance.get("totalSupply"))
        let totalSupplyNegatedBig = new BigNumber(dailyBalance.get("totalSupplyNegated"))
        let totalSupplyNotNegatedBig = new BigNumber(dailyBalance.get("totalSupplyNotNegated"))
        quarterlyAccountsAndBalancesDto.quarterlyTotalSupply = quarterlyAccountsAndBalancesDto.quarterlyTotalSupply.plus(totalSupplyBig)
        quarterlyAccountsAndBalancesDto.quarterlyTotalSupplyNegated = quarterlyAccountsAndBalancesDto.quarterlyTotalSupplyNegated.plus(totalSupplyNegatedBig)
        quarterlyAccountsAndBalancesDto.quarterlyTotalSupplyNotNegated = quarterlyAccountsAndBalancesDto.quarterlyTotalSupplyNotNegated.plus(totalSupplyNotNegatedBig)


        let accounts = dailyBalance.balances.keys()
        for (let account of accounts) {
            if (dbQuarterlyDividends.balances[account]) {
                let oldAccumulatedValue = new BigNumber(dbQuarterlyDividends.balances[account])
                let dailyValue = new BigNumber(dailyBalance.balances.get(account))
                let newAccumulatedValue = new BigNumber(oldAccumulatedValue.plus(dailyValue))
                dbQuarterlyDividends.balances[account] = newAccumulatedValue.toString(10)
            }
            else {
                let dailyValue = new BigNumber(dailyBalance.balances.get(account))
                dbQuarterlyDividends.balances[account] = dailyValue.toString(10)
            }
        }
    }
}

async function calculatePercentagesAndAmountsFromBalances(year, quarter) {
    let dbQuarterlyDividends = await QuarterlyDividends.findOne({ // TODO: This is unecessary, we can re-use from (buildQuarterlyDividendsObject)
        year: year,
        quarter: quarter
    })
    if (dbQuarterlyDividends !== null) {

        console.log("Building address to percentage mapping ...")
        let addressesToPercentagesMap = await buildAddressToPercentageMap(dbQuarterlyDividends)
        dbQuarterlyDividends.percentages = addressesToPercentagesMap

        await QuarterlyDividends.update({
            year: dbQuarterlyDividends.year,
            quarter: dbQuarterlyDividends.quarter
        }, dbQuarterlyDividends)
        console.log("Complete: Building address to percentage mapping ...")

        console.log("Building address to payout-amount mapping ...")
        let totalPayoutAmount = await getTotalAmountToPayOut(addressesToPercentagesMap)
        console.log("Total amount to pay out: ", totalPayoutAmount)
        let addressToAmountMap = await buildAddressToAmountMap(addressesToPercentagesMap, totalPayoutAmount)
        dbQuarterlyDividends.amounts = addressToAmountMap
        console.log("Payout datetime: ", Moment().add(1, 'days').format('DD-MM-YYYY'))
        dbQuarterlyDividends.payoutTime = Moment(new Date()).add(1, 'days').format('DD-MM-YYYY')
        console.log("Updating quarterly dividends: ", dbQuarterlyDividends)
        console.log("Complete: Building address to payout-amount mapping ...")
        await QuarterlyDividends.update({
            year: dbQuarterlyDividends.year,
            quarter: dbQuarterlyDividends.quarter
        }, dbQuarterlyDividends)

    } else {
        throw new Error("Couldn't retrieve quarterly dividends.")
    }
}

async function buildAddressToPercentageMap(quarterlyDividends) {

    let addressesToPercentages = {}

    for (let addressToBalance of quarterlyDividends.balances) {
        let address = addressToBalance[0]
        let balance = addressToBalance[1]

        // Check if negated, no negated account should be here.
        let isNegated = await Account.count({address: address, negated: true})
        if (isNegated) {
            addressesToPercentages[address] = 0
        }
        else {
            let totalSupplyNotNegatedBIG = new BigNumber(quarterlyDividends.totalSupplyNotNegated)
            let balanceBIG = new BigNumber(balance)
            let percentageBIG = balanceBIG.div(totalSupplyNotNegatedBIG)
            addressesToPercentages[address] = percentageBIG.toString(10);
            addressesToPercentages[address] = percentageBIG.toString(10);

        }
    }

    let controlNumberForPercentage = Object.values(addressesToPercentages).reduce((pre, cur) => {
        return (new BigNumber(pre)).plus(new BigNumber(cur))
    })

    /!*if (BigNumber(controlNumberForPercentage.toString()) !== 1) {
        console.log(controlNumberForPercentage);
        process.exit()
    }*!/

    console.log('addressesToPercentages: ',addressesToPercentages);
    return addressesToPercentages
}

async function buildAddressToAmountMap(addressToPercentageMap, totalAmount) {
    for (let address in addressToPercentageMap) {
        console.log("Total is", totalAmount, " Amount is", addressToPercentageMap[address])
        let totalAmountBig = new BigNumber(totalAmount)
        let percentageBig = new BigNumber(addressToPercentageMap[address])
        addressToPercentageMap[address] = Math.floor(totalAmountBig.times(percentageBig)) //TODO: Math.floor takes Number as argument. We're sending a BigNumber - may cause problems in the future?
    }
    return addressToPercentageMap
}

async function upsertQuarterlyDividendsToDb(quarterlyDividends) {
    let query = {year: quarterlyDividends.year, quarter: quarterlyDividends.quarter}
    let options = {upsert: true}
    await QuarterlyDividends.findOneAndUpdate(query, quarterlyDividends, options)
}

function getPreviousQuarterFromInputMoment(moment) {
    if (moment.quarter() === 1) {
        moment.year(moment.year() - 1)
        moment.quarter(4)
    } else {
        moment.quarter(moment.quarter() - 1)
    }

    return ({
        startDate: Moment(moment.startOf('quarter')),
        endDate: Moment(moment.endOf('quarter')),
        quarter: moment.quarter(),
        year: moment.year()
    })
}

async function getTotalAmountToPayOut() {
    return await Web3.eth.getBalance('0xa0ff1e0f30b5dda2dc01e7e828290bc72b71e57d')
    // return await Web3.eth.getBalance(process.env.ADDRESS)
}

// CALL FUNCTION WITH ADDRESS->WEI MAPPING, AND WILL PAY OUT TO ALL ADDRESSES
async function makePayments(quarterlyDividends) {
    let addressToAmountMap = quarterlyDividends.amounts
    let addresses = addressToAmountMap.keys()
    for (let address of addresses) {
        console.log("Payout to address: " + address + " with amount: " + addressToAmountMap.get(address))
        await transferWeiToAddress(address, addressToAmountMap.get(address), quarterlyDividends)
    }
}

//
async function transferWeiToAddress(address, amountInWei, quarterlyDividends) {
    if (0 === parseInt(amountInWei)) { // TODO: May error? amountInWei could be bigger than Integer can hold.
        return
    }
    let tx = {
        from: process.env.ADDRESS,
        to: Web3.utils.toChecksumAddress(address),
        value: amountInWei
    }

    let amountInWeiBIG = new BigNumber(amountInWei)
    let estimatedGas = await Web3.eth.estimateGas(tx)
    let estimatedGasBIG = new BigNumber(estimatedGas)
    let newTxValue = amountInWeiBIG.minus(estimatedGasBIG)
    tx.value = newTxValue.toString(10)
    tx.gas = estimatedGas

    console.log("Send total: ", Web3.utils.fromWei(String(amountInWei), 'ether'))
    console.log("Minus gas: ", Web3.utils.fromWei(String(tx.value), 'ether'))

    let signedTx = await Web3.eth.accounts.signTransaction(tx, process.env.PRIVATE_KEY)


    Web3.eth.sendSignedTransaction(signedTx.rawTransaction)
        .on('receipt', async function (receipt) { // TODO: Should function be async? Will it take longer executing all txs? Handle receipt in any other way?
            let payout = new Payout({
                address: tx.to,
                amount: amountInWei,
                year: quarterlyDividends.year,
                quarter: quarterlyDividends.quarter,
                date: new Date(),
                successful: true,
                message: ""
            })
            await payout.save()

        })
        .on('error', async function (error) { // TODO: Should function be async?
            console.log("error", error)
            let payout = new Payout({
                address: tx.to,
                amount: amountInWei,
                year: quarterlyDividends.year,
                quarter: quarterlyDividends.quarter,
                date: new Date(),
                successful: false,
                message: "" + error
            })
            await payout.save()
        })
}

async function sendMailQuarterly(previousQuarter) {
    await generateReportFromQuarterlyDividends(previousQuarter)

    let file = base64_encode('./quarterlyreports/' + getMostRecentFileName())
    const msg = {
        to: 'aa@akj.com',
        from: 'akjtoken@akj.com',
        subject: 'AKJ Token Quarterly Dividends Report and Payout Warning',
        html: "<h1>AKJ Token Quarterly Dividends Report</h1><p>The quarterly report on AKJ Token holders, their current balances, and their expected percentage and amounts to be paid out in dividends.</p><p>Please note that the payout will be executed in approximately 24 hours.</p>",
        attachments: [
            {
                filename: 'AKJToken_QuarterReport_' + transformToDateString(new Date()) + '.csv',
                content: file
            }
        ]
    }
    console.log("MAIL API", process.env.SENDGRID_API_KEY)
    await sgMail.send(msg)

    // function to encode file data to base64 encoded string
    function base64_encode(file) {
        var bitmap = fs.readFileSync(file)
        return new Buffer(bitmap).toString('base64')
    }

    function getMostRecentFileName() {
        let dir = './quarterlyreports'
        let files = fs.readdirSync(dir)

        return _.max(files, function (f) {
            let fullpath = path.join(dir, f)
            return fs.statSync(fullpath).mtime
        })
    }
}

async function generateReportFromQuarterlyDividends(previousQuarter) {
    let quarterlyDividends = await QuarterlyDividends.findOne({
        year: previousQuarter.year,
        // quarter: previousQuarter.quarter
        quarter: 4
    }) // Retrieve all accounts from "Date"

    console.log(previousQuarter.year, previousQuarter.quarter);

    let currentDate = new Date()
    let finalPathFile = "./quarterlyreports/quarterlyReport_" + previousQuarter.year + "-Q" + previousQuarter.quarter + "_" + currentDate.getTime() + ".csv"
    let records = []
    const createCsvWriter = require('csv-writer').createObjectCsvWriter

    const newCsvWriter = createCsvWriter({
        path: finalPathFile,
        header: [
            {
                id: 'address', title: 'address'
            },
            {
                id: 'balance', title: 'balance'
            },
            {
                id: 'percentage', title: 'percentage'
            },
            {
                id: 'amount', title: 'amount in ether'
            }
        ]
    })


    for (const address of quarterlyDividends.balances.keys()) {

        let balance_in_wei_big = new BigNumber(quarterlyDividends.balances.get(address)),
            balance_in_wei = balance_in_wei_big.toFixed(),
            balance_in_ether = Web3.utils.fromWei(balance_in_wei,'ether'),
            amount_in_wei_big = new BigNumber(quarterlyDividends.amounts.get(address)),
            amount_in_wei = amount_in_wei_big.toFixed(),
            amount_in_ether = Web3.utils.fromWei(amount_in_wei,'ether');

        // console.log(balance_in_ether, amount_in_ether);

        records.push({
            address: address,
            balance: balance_in_ether,
            percentage: quarterlyDividends.percentages.get(address),
            amount: amount_in_ether
        })
    }

    await newCsvWriter.writeRecords(records).then(function () {
        console.log("Done writing quarterly CSV report with rows = ", records.length)
    })
}

async function setProviders(...args) {
    for (let i = 0; i < args.length; i++) {
        await setProvider(args[i])
    }

    async function setProvider(arg) {
        arg.setProvider(Web3.currentProvider)
        if (typeof arg.currentProvider.sendAsync !== "function") {
            arg.currentProvider.sendAsync = function () {
                return arg.currentProvider.send.apply(
                    arg.currentProvider, arguments
                )
            }
        }
    }
}

function transformToDateString(date) {
    return ('0' + date.getDate()).slice(-2) + '-'
        + ('0' + (date.getMonth() + 1)).slice(-2) + '-'
        + date.getFullYear()
}
*/
