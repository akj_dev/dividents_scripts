var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var dailyBalances = new Schema({
    date: {
        type: String,
        required: true,
    },
    fullDate: {
        type: Date
    },
    fromBlock: {
        type: Number,
        required: true
    },
    toBlock: {
        type: Number,
        required: true
    },
    balances: {
        type: Map,
        of: String
    },
    totalSupply: {
        type: String
    },
    totalSupplyNegated: {
        type: String
    },
    totalSupplyNotNegated: {
        type: String
    }
});

var DailyBalances = mongoose.model('DailyBalances', dailyBalances);

module.exports = DailyBalances;
