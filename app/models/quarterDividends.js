var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var quarterDividendsSchema = new Schema({
    quarter: Number,
    year: Number,
    balances: {
        type: Map,
        of: String
    },
    percentages: {
        type: Map,
        of: String
    },
    amounts: {
        type: Map,
        of: String
    },
    totalSupply: String,
    totalSupplyNegated: String,
    totalSupplyNotNegated: String,
    payoutTime: String,
    hasBeenPaid: Boolean,
});

var QuarterDividends = mongoose.model('QuarterDividends', quarterDividendsSchema);

module.exports = QuarterDividends;
