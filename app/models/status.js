var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var statusSchema = new Schema({
    timeStart: Date,
    timeEnd: Date,
    date: String,
    fromBlock: String,
    toBlock: String,
    lastBlock: String,
    firstBlock: String,
    numberOfTx: Number,
    numberOfAccounts: Number,
    state: Number
});

// statusSchema.pre('save', function(next) {
//
// })

var Status = mongoose.model('Status', statusSchema);

module.exports = Status;