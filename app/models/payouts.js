var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var payoutSchema = new Schema({
    address: {
        type: String,
        required: true,
    },
    year: Number,
    quarter: Number,
    date: Date,
    amount: String,
    successful: Boolean,
    message: String
});

var Payout = mongoose.model('Payout', payoutSchema);

module.exports = Payout;