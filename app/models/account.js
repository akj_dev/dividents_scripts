var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var accountSchema = new Schema({
    address: {
        type: String,
        required: true,
        unique: true
    },
    // balance: {
    //     type: String,
    //     required: true
    // },
    date: {
        type: Date,
        // required: true
    },
    block: {
        type: Number,
        // required: true
    },
    negated: {
        type: Boolean,
        default: false
    }
});

var Account = mongoose.model('Account', accountSchema);

module.exports = Account;