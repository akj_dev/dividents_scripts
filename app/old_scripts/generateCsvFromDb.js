var fs = require('fs');
var csvWriter = require('csv-write-stream');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DailyBalances = require('../models/dailyBalances');
var Account = require('../models/account');


async function generateReportFromDailyBalance() {
    await mongoose.connect('mongodb://localhost/akjtoken');

    var dailyBalanceObject = await DailyBalances.findOne({date: transformToDateString(new Date())}); // Retrieve all accounts from "Date"

    let writer;
    let currentDate = new Date();

    let finalPathFile = "./balances/balances" + currentDate.getFullYear() + "-" + (currentDate.getMonth()+1) + "-" + currentDate.getUTCDate() + ".csv";

    if (!fs.existsSync(finalPathFile))
        writer = csvWriter({ headers: ["address", "balance"]});
    else
        writer = csvWriter({sendHeaders: false});

    writer.pipe(fs.createWriteStream(finalPathFile, {flags: 'a'}));

    for (const k of dailyBalanceObject.balances.keys()) {
        writer.write({
            address: k,
            balance: dailyBalanceObject.balances.get(k),
        });
    }
    writed.end();

    await mongoose.disconnect();
}

async function init() {
    await generateReportFromDailyBalance();

}

init();



async function getAccountsFromDb() {
    var accounts = await Account.find(); // Retrieve all accounts

    let writer;
    let currentDate = new Date();

    let finalPathFile = "./balances/balances" + currentDate.getFullYear() + "-" + (currentDate.getMonth()+1) + "-" + currentDate.getUTCDate() + ".csv";

    if (!fs.existsSync(finalPathFile))
        writer = csvWriter({ headers: ["address", "balance"]});
    else
        writer = csvWriter({sendHeaders: false});

    writer.pipe(fs.createWriteStream(finalPathFile, {flags: 'a'}));

    for (let account of Object.keys(accountsAndBalancesDictionary)) {
        let accountToSave = new Account({
            address: account,
            balance: accountsAndBalancesDictionary[account],
            block: blockNumber,
            date: date
        });

        if (accountIds.indexOf(accountToSave.id) === -1) {
            accountIds.push(accountToSave.id);
        }

        await accountToSave.save();

        writer.write({
            address: accountToSave.address,
            balance: accountToSave.balance,
        });
    }

    writer.end();
}

function transformToDateString(date) {
    return ('0' + date.getDate()).slice(-2) + '-'
        + ('0' + (date.getMonth()+1)).slice(-2) + '-'
        + date.getFullYear();
}