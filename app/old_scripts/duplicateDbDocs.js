const mongoose = require('mongoose')
const Moment = require('moment')

let DailyBalances = require('../models/dailyBalances')

test()

async function test() {
    await mongoose.connect('mongodb://localhost/akjtoken')

    let iterableMoment = new Moment("2018-09-01")

    //  find the relevant document
    let dailyBalanceCopy = await DailyBalances.findOne({}).sort({fullDate: 1}).limit(1)
    // let dailyBalanceCopy = await DailyBalances.find()

    let dailyBalanceCopyMoment = Moment(dailyBalanceCopy.date, "DD-MM-YYYY")
    // create iteration from october 1st up to relevant documents date
    while (iterableMoment.isBefore(dailyBalanceCopyMoment)) {
        // duplicate document but with different dates
        let toSave = new DailyBalances({
            date: Moment(iterableMoment).format("DD-MM-YYYY"),
            fullDate: Moment(iterableMoment).toDate(),
            fromBlock: dailyBalanceCopy.fromBlock,
            toBlock: dailyBalanceCopy.toBlock,
            balances: dailyBalanceCopy.balances,
            totalSupply: dailyBalanceCopy.totalSupply,
            totalSupplyNegated: dailyBalanceCopy.totalSupplyNegated,
            totalSupplyNotNegated: dailyBalanceCopy.totalSupplyNotNegated
        })
        console.log("Duplicating for date .. ", toSave.date)
        await toSave.save()
        console.log("Duplication complete.")
        iterableMoment.add(1, 'days')
    }

    // disconnect
    await mongoose.disconnect()
    console.log("Finished copying")
    process.exit()

}