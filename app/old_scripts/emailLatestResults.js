var nodemailer = require('nodemailer');
var fs = require('fs');
var path = require('path');
var _ = require('underscore');

function getMostRecentFileName() {
    let dir = './balances';
    var files = fs.readdirSync(dir);

    return _.max(files, function(f) {
        var fullpath = path.join(dir, f);
        return fs.statSync(fullpath).mtime;
    })
};

function sendMail() {
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'LRDKICKASS@gmail.com',
            pass: '5QRCA!BSr@1h'
        }
    });

    var mailOptions = {
        from: 'LRDKICKASS@gmail.com',
        to: 'jonas.foyn@gmail.com',
        subject: 'Latest AKJ Token transaction report',
        text: 'AKJ Token Report\nAttached you will find the latest report on AKJ Token transactions and balances.',
        attachments: {
            filename: 'AKJreport_'+transformToDateString(new Date())+'.csv',
            path: './balances/' + getMostRecentFileName()
        }
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
}

function transformToDateString(date) {
    return ('0' + date.getDate()).slice(-2) + '-'
        + ('0' + (date.getMonth()+1)).slice(-2) + '-'
        + date.getFullYear();
}

sendMail();