const mongoose = require('mongoose')

let Account = require('../models/account')
let Status = require('../models/status')
let DailyBalances = require('../models/dailyBalances')
var SHA256 = require("crypto-js/sha256");


const Moment = require('moment')
let someDictionary = {}
let totalSupply = 10000000

run()
async function run() {
    let someDate = Moment().startOf('year')
    await mongoose.connect('mongodb://localhost/akjtoken')
    let addresses = generateAddresses(20)

    while (someDate < Moment().endOf('year')) {
        let saveThis = new DailyBalances({
            date: someDate.format('DD-MM-YYYY'),
            fromBlock: 0,
            toBlock: 2987094,
            balances: addresses,
            totalSupply: totalSupply,
            totalSupplyNegated: totalSupply,
        })
        await saveThis.save()
        someDate.add(1, 'days');
    }
    await mongoose.disconnect()
}

function generateAddresses(amount) {
    for (let i=0; i<amount; i++) {
        let randomNumber = "" + SHA256(randomString())
        someDictionary[randomNumber] = totalSupply / amount
    }
    return someDictionary
}

function randomString() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}
