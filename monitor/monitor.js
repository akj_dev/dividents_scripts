const exec = require('child_process').exec;

dir = exec("geth --syncmode 'fast' --cache=1024 --rpc --rpcapi 'db,eth,net,admin,web3' --datadir '/blockchain/fast'" , function(err, stdout, stderr) {
    if (err) {
      // should have err.code here?
      console.log("err", err)
    }
    console.log("Stdout: ", stdout);
  });

  dir.on('exit', function (code) {
    console.log("Geth has exited with code", code)
  });

