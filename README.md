# AKJ scripts

## Dependencies: 

Remember to set up MongoDB 

Remember to set up ``` windows-build-tools ``` for Windows, or ``` build-essentials ``` for Linux. 

## Misc

Repository of some scripts that are developed for the revenue sharing platform. 

``` gethScripts.js ``` is a javascript loadscript for geth. 

Simple docs Link: 

https://docs.google.com/document/d/1vYUQQv14GreruprB1YRtH1qKw8pC_cWuXSY_PSa94w4/edit?usp=sharing
